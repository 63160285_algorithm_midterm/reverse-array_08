import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class reverse{
    public static void reverse(int arr[],int start,int end) {
        int temp;
        if (start >= end) {
            return;
        }
        temp = arr[start];
        arr[start] = arr[end];
        arr[end] = temp;
        reverse(arr, start+1, end-1);
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(new InputStreamReader(System.in));
        int num = input.nextInt();
        int arr[] = new int[num];

        for(int i=0;i<num;i++) {
            arr[i] = input.nextInt();
        }
        reverse(arr, 0, arr.length-1);
        String strArr = Arrays.toString(arr);
        System.out.println(strArr);
    }
}